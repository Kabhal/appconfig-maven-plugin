package com.tsquare.maven.plugins.appconfig.export.filters;

public class IncludePrefixKeyFilter implements KeyFilter {
   private final String includePrefix;

   public IncludePrefixKeyFilter(String includePrefix) {
      this.includePrefix = includePrefix;
   }

   @Override
   public boolean test(String key) {
      return key.startsWith(includePrefix);
   }

}
