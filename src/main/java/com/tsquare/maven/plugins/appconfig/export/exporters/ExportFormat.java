package com.tsquare.maven.plugins.appconfig.export.exporters;

public enum ExportFormat {
   CONSOLE, XML, PROPERTIES, CSV
}
