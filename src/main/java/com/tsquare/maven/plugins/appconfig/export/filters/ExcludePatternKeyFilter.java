package com.tsquare.maven.plugins.appconfig.export.filters;

import java.util.regex.Pattern;

public class ExcludePatternKeyFilter implements KeyFilter {
   private final Pattern excludePattern;

   public ExcludePatternKeyFilter(String excludePattern) {
      this.excludePattern = Pattern.compile(excludePattern);
   }

   @Override
   public boolean test(String key) {
      return !excludePattern.matcher(key).matches();
   }

}
