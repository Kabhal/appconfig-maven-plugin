package com.tsquare.maven.plugins.appconfig.documentation.exporters;

import java.nio.charset.Charset;
import java.nio.file.Path;

import org.apache.maven.plugin.logging.Log;

import com.tsquare.maven.plugins.appconfig.documentation.model.ConfigurationMetadata;

import io.github.swagger2markup.markup.builder.MarkupDocBuilder;
import io.github.swagger2markup.markup.builder.MarkupLanguage;

public class MarkdownDocumentation extends MarkupDocumentation {

   public static final String DOCUMENTATION_FILENAME_NO_PREFIX = "appconfig";

   private final Log log;
   private final Path documentationPath;

   public MarkdownDocumentation(Log log,
                                ConfigurationMetadata metadata,
                                boolean external,
                                boolean toc,
                                Path documentationPath) {
      super(metadata, external, toc);

      this.log = log;
      this.documentationPath = documentationPath;
   }

   @Override
   public void documentation() {
      MarkupDocBuilder markup = markup(MarkupLanguage.MARKDOWN);

      Path documentationFile =
            markup.addFileExtension(documentationPath.resolve(DOCUMENTATION_FILENAME_NO_PREFIX));

      log.info(String.format("-- Generate Markdown configuration documentation to '%s'", documentationFile));

      markup.writeToFileWithoutExtension(documentationFile, Charset.defaultCharset());
   }
}
